Warehouse Operations Suite  
====================


.. include:: image.rst
.. include:: icon.rst


|global_screen|

Introduction
============

SIMOGGA WOS (Warehouse Optimization System) est un outil d’analyse et de
simulation destiné aux entrepôts. L’analyse se fait général en deux temps :

-   Analyse de la situation existante

-   Analyse de situations alternatives (ex : autre layout des emplacements de
    stockage, utilisation d’engins de transport de plus forte capacité, …)

En pratique, SIMOGGA WOS s’interface de façon très simple avec la plupart des
systèmes WMS (Warehouse Management System) présents sur le marché. Une fois
intégré, SIMOGGA WOS permet de transformer l’énorme quantité de données
contenues dans les WMS en informations claires et précises.

Une question business sur le fonctionnement de votre entrepôt ? SIMOGGA WOS va
vous aider !

Procédure d’installation
========================

Fichier de settings
-------------------

Placer le fichier ci-joint “settings.json” dans le répertoire :

“%HOMEPATH%\\AppData\\Roaming\\AmiaSystems\\Simogga\\settings”

Python
------

Télécharger (<https://www.python.org/downloads/>) et installer la dernière
version de Python 3.

-   Pendant l’installation, cocher la case « installer dans Path »

Pour vérifier l’installation de Python :

-   Ouvrir cmd.exe

-   Taper « python » et appuyer sur enter

-   En cas d’erreur le message « la commande n’est pas reconnue » apparaît.

-   En cas de succès le message suivant apparaît :

![](media/4ef29d52adbe4fac77d245906f073ca5.png)

Figure 1 : Python correctement installé

Installer la librairie Python suivante : xlrd

-   Ouvrir « cmd.exe »

-   Taper « pip install xlrd » et appuyer sur enter (en étant connecté à
    internet)

![](media/ef8e307a286f99b96431aa0d9e7b42cb.png)

Figure 2 : installation de la librairie XLRD

-   L’installation est finalisée quand le message suivant apparaît :

    ![](media/97ce8da3ec6adfaa0f168d55d3facde7.png)

Figure 3 : librairie XLRD correctement installée

Licence
-------

Obtenir et installer une licence pour SIMOGGA WOS

Prérequis
=========

![](media/71aedb6031daba41f55e824f028083e0.png)

Figure 4 : mode d'affichage "simulation"

Scénario
--------

Un scénario de SIMOGGA WOS se compose de 4 données d’input :

-   Le layout de l’entrepôt, composé d’allées et d’emplacement de stockage

-   Les opérateurs pouvant travailler dans l’entrepôt

-   Les type de produits pouvant être stockés

-   Les déplacements de produits ayant eu lieu pendant une période donnée

De surcroit, de nombreux paramètres de configuration sont disponibles (ex :
vitesse de déplacement des opérateurs, capacité maximale de produits pouvant
être déplacés en même temps, …)

SIMOGGA WOS vous permet de créer et de dupliquer autant de scenarios que
nécessaire. Tous ces différents scénarios pourront bien sûr être comparés entre
eux selon de nombreux critères d’évaluation.

Mode d’affichage
----------------

Trois modes d’affichage sont disponibles :

-   Layout : permet de visualiser et de modifier les allées de l’entrepôt. Les
    allées sont utilisées par les opérateurs pour se déplacer dans l’entrepôt.

-   Data : permet de visualiser et d’éditer les données « fixes » de l’entrepôt
    (= les emplacements de stockage, les produits et les opérateurs)

-   Simulation : pour un ensemble donné de déplacements de produits, permet de
    simuler, de quantifier et d’analyser le travail effectué par chacun des
    opérateurs.

Emplacement de stockage
-----------------------

Les emplacements de stockage représentent tous les endroits de l’entrepôt où les
opérateurs peuvent charger et décharger des produits. Ces emplacements de
stockage comprennent donc les emplacements de stockage de longue durée, mais
aussi les emplacements temporaires, comme des zones de déchargement de camions
ou des zones d’emballage.

Chaque emplacement de stockage est décrit par de nombreux paramètres
(coordonnées X Y Z, capacité de stockage maximale, …)

Flux opérateur
--------------

Un flux opérateur entre deux emplacements A et B de l’entrepôt représente
l’ensemble des déplacements d’opérateurs ayant eu lieu entre A et B pendant une
période donnée. Autrement dit, les flux opérateurs permettent facilement de
visualiser les allées (ou portions d’allées) les plus utilisées par les
opérateurs.

Guide de prise en main
======================

Arborescence des dossiers
-------------------------

Dans son mode de fonctionnement standard, une configuration de SIMOGGA WOS
contient les dossiers suivants :

-   Dossier « input » : contient l’ensemble des fichiers de données nécessaires
    au fonctionnement de SIMOGGA WOS.

-   Dossier « output » : contient les fichiers de résultats générés par SIMOGGA
    WOS.

-   Dossier « scripts » : contient différents scripts permettant d’importer les
    fichiers de données dans SIMOGGA WOS.

Définition des données
----------------------

Pour fonctionner de manière optimale, SIMOGGA WOS nécessite 4 fichiers de
données distincts, tous placés dans le répertoire « input » de la configuration
SIMOGGA. Chacun de ces fichiers doit respecter un format et un nom de fichier
bien spécifiques.

### Layout de l’entrepôt 

-   Type de fichier : Excel 2003 (extension « .xls »)

-   Nom de fichier : map.xls

La définition d’un layout d’entrepôt supporte 4 types d’éléments :

-   Le contour extérieur de l’entrepôt. Défini par un ensemble de cellules Excel
    dotées d’une couleur de fond noire.

-   Un emplacement de stockage : défini par une cellule Excel contenant du texte
    mis en gras. Le texte contenu dans la cellule servira à nommer l’emplacement
    de stockage lors de l’import dans SIMOGGA WOS.

-   Un espace occupé, mais non destiné à du stockage (ex : des sanitaires, un
    bureau, …) : défini par une cellule Excel contenant du texte (sans mise en
    gras). Le texte contenu dans la cellule servira à nommer la zone lors de
    l’import dans SIMOGGA WOS.

-   Un carrefour d’allée : défini par une cellule Excel contenant le texte
    « P ». Lors de l’import dans SIMOGGA WOS, tous les carrefours d’allée
    voisins seront reliés par des allées rectilignes.

![](media/c5c2551bd922a7f147714b5c0a55ca3f.png)

Figure 5 : layout d’entrepôt en Excel, cas simple

Remarques :

-   La largeur des colonnes et la hauteur des lignes sont prises en compte lors
    de l’import.

-   La fusion des cellules est respectée lors de l’import. Ainsi si 10 cellules
    Excel ont été fusionnées en une seule grande cellule, la procédure d’import
    ne créera qu’un seul élément dans SIMOGGA WOS ; cet élément respectera les
    dimensions de la cellule fusionnée.

-   La couleur de fond des cellules Excel n’est pas prise en compte lors de
    l’import, exception faite pour la couleur de fond noire.

-   Si le fichier Excel contient plusieurs onglets, seul le premier onglet sera
    pris en compte lors de l’import des données.

-   Ne pas mettre d’espaces et des caractères spéciaux pour le nommage (zone,
    espace de stockage, …)

![](media/870feb928add07472b60a268aadebbdc.png)

Figure 6 : layout d'entrepôt en Excel, cas plus complexe

### Type de produits

-   Type de fichier : texte avec colonnes. Séparateur de colonne : une virgule
    (extension « .csv »)

-   Nom de fichier : products.csv

Fichier détaillant l’ensemble des types de produits stockables dans l’entrepôt.
Actuellement, la définition d’un type de produit est très simple, seul son code
doit être spécifié.

La première ligne du fichier est destinée à contenir les en-têtes des
différentes colonnes. L’unique colonne actuelle est « Product code ».

Exemple d’un fichier valide :

| Product code      |
|-------------------|
| Pomme Poire Prune |

### Opérateurs

-   Type de fichier : texte avec colonnes. Séparateur de colonne : une virgule
    (extension « .csv »)

-   Nom de fichier : workers.csv

Fichier détaillant l’ensemble des opérateurs pouvant travailler dans l’entrepôt.
Actuellement, la définition d’un opérateur est très simple, seul son code doit
être spécifié.

La première ligne du fichier est destinée à contenir les en-têtes des
différentes colonnes. L’unique colonne actuelle est « Operator code ».

Exemple d’un fichier valide :

| Operator code      |
|--------------------|
| Roger Kevin Gaston |

### Déplacements de produits

-   Type de fichier : texte en colonnes. Séparateur de colonne : un
    point-virgule (extension « .csv »)

-   Nom de fichier : au choix (mais sans caractères spéciaux. Ex ; pas de
    « é ».)

Fichier détaillant un ensemble de déplacements de produits au sein de
l’entrepôt. Chaque déplacement de produit nécessite 11 informations :

| **Nom de la colonne** | **Description**                                                                                                                                        | **Format**                     | **Exemple** |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------|-------------|
| Operation type        | PICKING : sortie de stock d’un produit PUT_AWAY : entrée en stock d’un produit RELOCATE : déplacement interne d’un produit IDLE : opérateur en attente | PICKING PUT_AWAY RELOCATE IDLE | PICKING     |
| Worker code           | Code de l’opérateur effectuant l’opération                                                                                                             | Texte                          | Roger       |
| Start date            | Date de début de l’opération                                                                                                                           | yyyy-mm-dd                     | 2017-09-01  |
| Start time            | Heure de début de l’opération                                                                                                                          | HH:MM                          | 12:15       |
| End date              | Date de fin de l’opération                                                                                                                             | yyyy-mm-dd                     | 2017-09-01  |
| End time              | Heure de fin de l’opération                                                                                                                            | HH:MM                          | 12:30       |
| From location code    | Emplacement d’origine du produit                                                                                                                       | Texte                          | A001        |
| To location code      | Emplacement de destination du produit                                                                                                                  | Texte                          | OUT         |
| Product code          | Code du type de produit déplacé                                                                                                                        | Texte                          | Prune       |
| Product quantity      | Quantité de produit déplacé                                                                                                                            | Nombre entier                  | 40          |
| Product batch code    | Information non encore utilisée                                                                                                                        | Texte                          | 1           |

Exemple d’un fichier valide :

| Operation type,Worker code,Start date,Start time,End date,End time,From location code,to location code,Product quantity,Product batch code                                                                   |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| PICKING,Roger,2017-09-01,12:15,2017-09-01,12:30,A001,OUT,Prune,40,1 PICKING,Roger,2017-09-01,12:30,2017-09-01,12:45,A009,OUT,Pomme,25,1 PICKING,Roger,2017-09-01,12:45,2017-09-01,13:00,A015,OUT,Poire,100,1 |

Attention, Il ne peut pas y avoir de caractère spécial dans le nom du fichier.
Ex : « Février.csv » ne passera pas. Il faut nommer le document « Fevrier.csv »

Import des données
------------------

Une fois les 4 fichiers de données correctement remplis et placés dans le
répertoire « input », leur import dans SIMOGGA WOS se fait aisément :

-   Démarrer SIMOGGA WOS

-   Dans le menu « fichier », sélectionner l’action « Ouvrir… »

-   Dans le dossier « scripts », sélectionner le fichier « import layout -
    products - workers.lua »

-   Confirmer l’ouverture du fichier et attendez quelques secondes…

-   C’est terminé, vos données sont maintenant importées dans SIMOGGA WOS !

Les deux figures ci-dessous illustrent le résultat de l’import du layout de
l’entrepôt défini sous Excel en Figure 4.

![](media/04347763f55372e3d7eb760dcdee8e57.png)

Figure 7 : layout d’entrepôt, cas simple (vue layout)

En Figure 7, nous voyons (en rouge) les 6 carrefours d’allée définis sous Excel
et toutes les allées qui désormais les relient.

En Figure 8, nous voyons les mêmes données, mais depuis la vue « data ». Les
allées ne sont plus visibles dans cette vue mais tous les emplacements de
stockage sont désormais visibles. On constate que l’ensemble des emplacements
définis dans le fichier Excel ont été correctement importés et placés sur le
plan à la position attendue.

![](media/924d0676fccdc8b0eef8fce484f488c0.png)

Figure 8 : layout d’entrepôt, cas simple (vue data)

Remarques :

-   Cette étape d’import ne porte en réalité que sur 3 fichiers de données parmi
    les 4 existants. Le fichier des déplacements de produits reste encore à
    importer. Cette opération est détaillée dans la section 4.4.2.

-   L’import des données ne doit pas être effectué à chaque démarrage de
    SIMOGGA. Une fois les données correctement importées, le cas courant peut
    être sauvé dans un fichier « .db » via l’action « save as » du menu
    « fichier ». Lors du prochain démarrage de SIMOGGA WOS, il suffira de
    charger ce fichier via l’action « ouvrir… » du menu « fichier ».

Simulation
----------

La phase d’import terminée, une ou plusieurs simulations vont pouvoir être
effectuées sur l’entrepôt défini dans SIMOGGA WOS. Pour accéder aux
fonctionnalités de simulation, cliquer sur le bouton « Simulation » de la barre
d’outils supérieure. Une fois ce mode d’affichage activé, la barre d’outils
inférieure se met à jour, comme illustré en Figure 9.

![](media/28eddb3060fdc6ef484bcc90857f5c4c.png)

Figure 9 : barre d'outils du mode "simulation"

Cette nouvelle barre d’outils va nous permettre de construire la simulation pas
à pas. Détaillons chacune des étapes, ainsi que les différents boutons et
composants présents dans la barre d’outils (en partant du bord gauche de
l’écran) :

### Gestion de plusieurs simulations, simulation courante

Eléments de la barre d’outils concernés : la combo box (contenant par défaut
« WOS 1 »), le bouton « Delete » et le bouton « New » 

Ils permettent de définir plusieurs simulations et de choisir sur quelle
simulation l’on souhaite travailler. Pour le moment, nous allons simplement
utiliser la simulation « WOS 1 », présente par défaut lorsqu’un cas est chargé
dans SIMOGGA WOS.

### Import des déplacements de produits

Elément de la barre d’outils concerné : le bouton « Data »

Un clic sur ce bouton ouvre un dialogue qui nous permet de sélectionner un
fichier contenant un ensemble de déplacements de produits (autrement dit le 4ème
et dernier fichier de données qu’il nous restait à importer). Une fois l’import
terminé, la liste complète des déplacements de produits est affichée dans le
dialogue, comme illustré en Figure 10.

![](media/46e73d587451eb120d3d3db89d39e94c.png)

Figure 10 : liste des déplacements de produits

### Paramétrage de la simulation

Elément de la barre d’outils concerné : le bouton « Config »

Un clic sur ce bouton ouvre un dialogue qui nous permet de contrôler les
différents paramètres de la simulation (voir Figure 11).

![](media/1076a200f50e0e89a40a85a7fc6368f3.png)

Figure 11 : paramétrage de la simulation

Détaillons chacun de ces paramètres :

| **Nom du paramètre**  | **Description**                                                                                                                                                                                                |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Max op. before unload | Nombre maximal de chargements de produits pouvant être effectué consécutivement par un opérateur. Une fois cette limite atteinte, l’opérateur est obligé d’aller décharger tous les produits qu’il transporte. |
| Picking (sec.)        | Nombre de secondes nécessaires à un opérateur pour charger un produit                                                                                                                                          |
| Unload (sec.)         | Nombre de secondes nécessaires à un opérateur pour décharger un produit                                                                                                                                        |
| Speed (km/h)          | Vitesse de déplacement d’un opérateur (qu’il transporte ou non des produits)                                                                                                                                   |

### Exécution de la simulation

Elément de la barre d’outils concerné : le bouton « Run »

Un clic sur ce bouton démarre l’exécution de la simulation. Selon la quantité de
déplacements de produits importés, cette action peut être instantanée ou durer
quelques secondes.

La simulation consiste à exécuter en détails chacun des déplacements de produits
chargés dans la simulation courante (voir 4.4.2). Plus précisément, chaque
déplacement va être décomposé en quatre sous-étapes. Pour les illustrer, prenons
le déplacement de produit suivant :

PICKING,Roger,2017-09-01,12:15,2017-09-01,12:30,A001,OUT,Prune,40,1

-   Sous-Étape 1 : déplacement de l’opérateur « Roger » de son emplacement
    actuel à l’emplacement « A001 »

-   Sous-étape 2 : chargement par l’opérateur « Roger » de 40 unités du produit
    « Prune »

-   Sous-étape 3 : déplacement de l’opérateur « Roger » de son emplacement
    actuel (« A001 ») à l’emplacement « OUT »

-   Sous-étape 4 : déchargement par l’opérateur « Roger » de 40 unités du
    produit « Prune »

Remarques :

-   Quand un opérateur a chargé un produit, il ne doit pas forcément aller le
    décharger immédiatement. A la place, il peut décider d’aller charger le
    produit suivant. Des chargements consécutifs ne sont toutefois possibles que
    si :

    -   Tous les produits chargés doivent être déchargés au même emplacement.

    -   La capacité de chargement maximale de l’opérateur n’est pas dépassée
        (voir paramètre « Max op. before unload » en 4.4.3)

-   Pour analyser en détails toutes les sous-étapes générées par le moteur de
    simulation, vous pouvez retourner dans le dialogue des déplacements de
    produits (voir 4.4.2), puis cliquer sur le second onglet « Output » (voir
    Figure 12)

![](media/8f845ad4d1afac34ed2e51572458659d.png)

Figure 12 : déplacements de produit, détails des sous-étapes

### Analyse

Eléments de la barre d’outils concerné :

-   Analyse par tableau : les bouton « Summary » et « Analyze ». Expliqués en
    détails en sections 4.5.1 et 4.5.2.

-   Analyse visuelle : les boutons « Flow » et « Color ». Expliqués en détails
    en sections 4.5.3 et 4.5.4.

Analyse et comparaison des simulations
--------------------------------------

Une fois les différentes simulations effectuées, SIMOGGA WOS met à la
disposition de l’utilisateur toute une série d’outils permettant :

-   L’analyse poussée d’une simulation donnée

-   La comparaison de plusieurs simulations selon différents critères

Détaillons maintenant chacun de ces outils.

### Par tableau simple

Ce tableau, accessible via le bouton « Summary », permet de comparer très
rapidement toutes les simulations effectuées. Nous y trouvons une ligne par
simulation et une colonne par indicateur. Les indicateurs sont les suivants :

-   « Distance » : la distance totale parcourue par les opérateurs pour
    effectuer l’ensemble des déplacements de produits importés dans la
    simulation.

-   « Working time (input) » : la durée totale nécessaire aux opérateurs pour
    déplacer tous les produits. Cette durée est uniquement calculée sur base des
    données d’input :

    1.  Pour chaque déplacement de produit, on calcule sa durée comme suit :
        heure de fin – heure de début

    2.  On somme toutes les durées calculées

-   « Working time (computed) » : la durée totale nécessaire aux opérateurs pour
    déplacer tous les produits, mais cette fois calculée sur base de la
    simulation. Cette durée est donc calculée en additionnant l’ensemble des
    durées de chargement, de déchargement et de déplacement des opérateurs.

-   « Cost » : représente le coût opérationnel total pour effectuer l’ensemble
    des déplacements de produits. Indicateur pas encore disponible dans la
    version actuelle de SIMOGGA WOS.

![](media/229a42195b1a43453a89168f3473da00.png)

Figure 13 : analyse de simulations, tableau simple

### Par tableau dynamique

Ces tableaux, accessibles via le bouton « Analyze », permettent à l’utilisateur
de paramétrer avec une grande flexibilité les informations qu’il souhaite
analyser et comparer. La Figure 14 illustre notre outil de tableaux dynamiques ;
il se découpe en 4 parties :

1.  Les différents tableaux dynamiques existants, un par onglet. Tout à droite,
    un bouton « + » permet de créer de nouveaux onglets.

2.  Permet de définir la précision du tableau (par heure, jour ou semaine),
    ainsi que la période concernée. Seuls les déplacements de produits inclus
    dans la période définie seront pris en compte lors de la construction du
    tableau.

3.  Les différents éléments pouvant servir à la construction du tableau. L’ordre
    des éléments peut être modifié via un « drag-and-drop » (« Scenarios » peut
    par exemple être placé avant « KPIs »). Les éléments actuellement
    disponibles sont :

    1.  KPIs : analyse selon chacun des trois KPIs déjà utilisés par les
        tableaux simples (voir 4.5.1)

    2.  Scenarios : analyse selon chaque scénario WOS défini

    3.  Workers : analyse selon chaque opérateur travaillant dans l’entrepôt

    4.  Stocks : analyse selon chaque sous-ensemble d’emplacements de stockage
        défini

    5.  Activities : analyse selon chaque type d’activité que les opérateurs
        peuvent effectuer dans l’entrepôt

4.  Le tableau dynamique, construit selon les critères définis dans les parties
    A, B et C. Les valeurs des cellules d’une ligne du tableau ne peuvent
    toutefois être calculées que si :

    1.  **Un** type de KPI est défini.

    2.  **Un** scénario est défini.

![](media/e9412407d7bb2d678f3670ba10517f1f.png)

Figure 14 : analyse de simulations, tableau dynamique

### Par flux opérateur

Ce mode de visualisation, accessible via le bouton « Flow », permet d’afficher
le « flux opérateur » (voir 3.4) de chacune des allées de l’entrepôt. Il est
particulièrement utile pour détecter :

-   Des éventuelles congestions dans le trafic représentant l’ensemble des
    déplacements des opérateurs.

-   A l’inverse, des allées (ou portions d’allées) très peu utilisées par les
    opérateurs.

![](media/8b2e30032e791f44e457b81ed3b5e516.png)

Figure 15 : analyse de simulations, flux opérateur

### Par « heat map »

Ce mode de visualisation, accessible via le bouton « Color », permet d’afficher
les emplacements de stockage selon 3 couleurs :

-   Rouge : emplacement de stockage fortement utilisé

-   Vert : emplacement de stockage moyennement utilisé

-   Bleu : emplacement de stockage faiblement utilisé

Plus précisément, l’assignation des couleurs fonctionne comme suit :

1.  Pour chaque emplacement de stockage, on calcule son degré d’utilisation,
    c.-à-d. le nombre de chargements et de déchargements de produits que les
    opérateurs y ont effectué.

2.  On trie l’ensemble des emplacements de stockage par degré d’utilisation.

3.  Le premier tiers des emplacements sont colorisés en bleu, le second tiers en
    vert et le dernier tiers en rouge.

![](media/cbf59880b5c378017a60962cb4bd0c13.png)

Figure 16 : analyse de simulations, heat map