Glossary
========

.. glossary::
	:sorted:
	
	Flow
		Number of movements or amount of products transferred between two machines (From - To).
		A flow is expressed in terms of movement and quantity.

	Graphical View 
		The graphical view shows the directional flow (From - To)
		The graphical view allows visualization of (technical cultural, historical) directional flow freely

	Real View
		The actual view is summed flows on each segment without steering precision
		The actual  includes the plant layout to accommodate the technical requirements of the plant (plant areas, entry-exit, immovable machinery)

	Movement (Flows))
		A movement is a transfer of a batch of parts from a position A to a position B)
		The flow value is the number of trips made to move all parts of a position A to a position B

	Quantity (Flows))
		The flow value is the sum of all parts moved from a position A to a position B

	Machine
		SIMOGGA considered a machine, machine, workstation, storage where the product is stopped, (it can be transformed by an operation of the process or just stored for some time).
		The machines have a **color** characterizes the type of machine
		The filling **level** of the colored part is the percentage of the load with respect to the ability

	Views
		SIMOGGA is organized into different views: real view and graphical view.  

	Alternative
		They used to define situations where machines are moved. This allows, within a scenario to build the successive steps to be followed to arrive at the optimal situation
		An alternative may be seen as a picture, a backup at some point. Just duplicate the alternative being to continue the analysis process and maintain the current status.

	Scenario
		An Operation-Machine solution that involves flow between the various machines and a specific machine utilization (load relative to the defined capacity).
		Each scenario is characterized by a factory design.

	Scene
		The scene sets the graphic display area.

