.. -----------------------------prise_en_main-----------------------------------
.. |add_blueprint| image:: _static/img/quick_start_guide/add_blueprint.png
.. |insert_zone| image:: _static/img/quick_start_guide/insert_zone.png
.. |open_file| image:: _static/img/quick_start_guide/open_file.png
.. |raw_material| image:: _static/img/quick_start_guide/raw_material.png
.. |set_as_baseline| image:: _static/img/quick_start_guide/set_as_baseline.png
.. |crop_cell| image:: _static/img/quick_start_guide/crop_cell.png
.. |insert_cell| image:: _static/img/quick_start_guide/insert_cell.png
.. |resize_zone| image:: _static/img/quick_start_guide/resize_zone.png
.. |sample_machine| image:: _static/img/quick_start_guide/sample_machine.png
.. |set_scaleQS| image:: _static/img/quick_start_guide/set_scaleQS.png
.. |turn_zone| image:: _static/img/quick_start_guide/turn_zone.png

.. |by products| image:: _static/img/quick_start_guide/by products.png
.. |by_machine_filters| image:: _static/img/quick_start_guide/by_machine_filters.png
.. |cell_context_menu| image:: _static/img/quick_start_guide/cell_context_menu.png
.. |change_mode_interaction| image:: _static/img/quick_start_guide/change_mode_interaction.png
.. |dashboard| image:: _static/img/quick_start_guide/dashboard.png
.. |filters_menu| image:: _static/img/quick_start_guide/filters_menu.png
.. |line_context_menu| image:: _static/img/quick_start_guide/line_context_menu.png
.. |machine_context| image:: _static/img/quick_start_guide/machine_context.png

.. |prise_en_main| image:: _static/img/prise_en_main_rapide_en.png
.. |quick_start| image:: _static/img/quick_start_en.png
.. |quick_start_1| image:: _static/img/quick_start_1_en.png
.. |quick_start_2| image:: _static/img/quick_start_2_en.png
.. |quick_start_3| image:: _static/img/quick_start_3_en.png
.. |quick_start_4| image:: _static/img/quick_start_4_en.png
.. |global_screen| image:: _static/img/global_screen_en.png


.. list of all pictures / Fichier excel
.. |productsEx| image:: _static/img/excel_products_en.png
.. |machines| image:: _static/img/excel_machines_en.png
.. |machine_type| image:: _static/img/excel_machine_type_en.png
.. |message_load_case| image:: _static/img/message_load_case_en.png

.. list of all pictures / Cr�ation de la situation As-IS
.. |design_selection| image:: _static/img/mode_design_selection_en.png
.. |interaction_selection| image:: _static/img/mode_interaction_selection_en.png
.. |scenario| image:: _static/img/scenario_active_en.png
.. |alternative| image:: _static/img/alternative_active_gv_en.png
.. |set_as_is_situation| image:: _static/img/alternative_asis_en.png
.. |set_as_is_situation_by_scenario| image:: _static/img/menu_scenario_set_as_is_en.png
.. |set_scale| image:: _static/img/set_scale_en.png

.. list of all pictures / Analyse graphique des flux
.. |filter_by_machine| image:: _static/img/box_filter_machine_en.png
.. |filter_by_product| image:: _static/img/box_filter_product_en.png
.. |filter_by_product_prod| image:: _static/img/box_filter_product_product_en.png
.. |filter_by_family| image:: _static/img/box_filter_product_family_en.png
.. |filter_pareto| image:: _static/img/box_filter_product_pareto_en.png
.. |filter_info| image:: _static/img/box_filter_product_info_en.png
.. |visualisation| image:: _static/img/panel_lateral_filter_en.png
.. |visualisation_first_step| image:: _static/img/visualisation_first_step_en.png
.. |visualisation_next_step| image:: _static/img/visualisation_next_step_en.png
.. |extend_the_scale| image:: _static/img/extend_scale_en.png
.. |flow_decomposition| image:: _static/img/box_rerouting_en.png
.. |optimizer_open| image:: _static/img/box_optimizer_open_en.png
.. |optimizer| image:: _static/img/box_optimizer_en.png

.. list of all pictures / G�n�ration de sc�narios
.. |evaluation_positive| image:: _static/img/evaluation_positive_en.png
.. |evaluation_negative| image:: _static/img/evaluation_negative_en.png
.. |use_scenarios| image:: _static/img/use_scenarios.png

.. -----------------------------main principal-----------------------------------

.. list of all pictures // menu_principal

.. list of all pictures / File
.. |menu_file| image:: _static/img/menu_file_en.png
.. |menu_file_open| image:: _static/img/menu_file_open_en.png
.. |menu_file_save| image:: _static/img/menu_file_save_en.png

.. |menu_file_save_as| image:: _static/img/menu_file_saveas_en.png
.. |menu_file_close| image:: _static/img/menu_file_close_en.png
.. |menu_file_quit| image:: _static/img/menu_file_quit_en.png

.. list of all pictures / Option
.. |menu_option| image:: _static/img/menu_options_en.png
.. |menu_option_conf| image:: _static/img/menu_options_conf_en.png
.. |menu_option_reroutage| image:: _static/img/menu_options_rerouting_en.png
.. |menu_option_filter| image:: _static/img/menu_options_filter_en.png

.. list of all pictures / scenario
.. |menu_scenario| image:: _static/img/menu_scenario_en.png
.. |menu_scenario_new_sce| image:: _static/img/menu_scenario_new_sce_en.png
.. |menu_scenario_dup_sce| image:: _static/img/menu_scenario_dup_sce_en.png
.. |menu_scenario_del_sce| image:: _static/img/menu_scenario_del_sce_en.png
.. |menu_scenario_new_alt| image:: _static/img/menu_scenario_new_alt_en.png
.. |menu_scenario_dup_alt| image:: _static/img/menu_scenario_dup_alt_en.png
.. |menu_scenario_del_alt| image:: _static/img/menu_scenario_del_alt_en.png
.. |menu_scenario_as_is| image:: _static/img/menu_scenario_set_as_is_en.png

.. list of all pictures / optimizer
.. |menu_optimizer| image:: _static/img/menu_optimizer_en.png
.. |menu_optimizer_cell| image:: _static/img/menu_opt_cell_en.png
.. |menu_optimizer_apply| image:: _static/img/menu_opt_apply_en.png

.. -----------------------------menu_lateral-----------------------------------

.. list of all pictures / menu_lateral
.. |panel_lateral| image:: _static/img/panel_lateral_en.png
.. |panel_lateral_scenario| image:: _static/img/panel_lateral_scenario_en.png
.. |scenario_right_click| image:: _static/img/scenario_en.png
.. |alternative_right_click| image:: _static/img/alternative_en.png
.. |panel_lateral_structure| image:: _static/img/panel_lateral_structure_en.png
.. |panel_lateral_info| image:: _static/img/panel_lateral_info_en.png
.. |panel_lateral_dashboard| image:: _static/img/panel_lateral_dashboard_en.png

.. |panel_lateral_optsc| image:: _static/img/panel_lateral_optsc_en.png
.. |view_optsc| image:: _static/img/view_optsc_en.png

.. |view_graph| image:: _static/img/view_graph_en.png
.. |view_graph_0| image:: _static/img/view_graph_0.png
.. |view_graph_1| image:: _static/img/view_graph_1.png
.. |view_graph_2| image:: _static/img/view_graph_2.png
.. |view_graph_3| image:: _static/img/view_graph_3.png
.. |view_graph_4| image:: _static/img/view_graph_4.png
.. |view_graph_5| image:: _static/img/view_graph_5.png
.. |panel_filter_1| image:: _static/img/panel_lateral_filter_1_en.png
.. |panel_filter_2| image:: _static/img/panel_lateral_filter_2_en.png

.. |panel_filter| image:: _static/img/panel_lateral_filter_en.png
.. |panel_filter_60-100| image:: _static/img/panel_lateral_filtre_60-100_en.png
.. |panel_filter_25-100| image:: _static/img/panel_lateral_filtre_25-100_en.png
.. |panel_filter_0-25| image:: _static/img/panel_lateral_filtre_0-25_en.png
.. |panel_filter_0-25_magn| image:: _static/img/panel_lateral_filtre_0-25_magn_en.png
.. |panel_filter_0-100_view| image:: _static/img/panel_lateral_filtre_0-100_view.png
.. |panel_filter_60-100_view| image:: _static/img/panel_lateral_filtre_60-100_view.png
.. |panel_filter_25-100_view| image:: _static/img/panel_lateral_filtre_25-100_view.png
.. |panel_filter_0-25_view| image:: _static/img/panel_lateral_filtre_0-25_view.png
.. |panel_filter_0-25_view_magnify| image:: _static/img/panel_lateral_filtre_0-25_magnify_view.png
.. |panel_thickness| image:: _static/img/thickness_en.png



.. -----------------------------menu vue-----------------------------------

.. list of all pictures / menu_vue / graphique
.. |panel_graphic| image:: _static/img/panel_view_graphic_en.png

.. list of all pictures / menu_vue / sur plan/Reelle
.. |panel_real_interaction| image:: _static/img/panel_view_real_interaction_en.png
.. |panel_real_design| image:: _static/img/panel_view_real_design_en.png

.. -----------------------------menu_sur_vue-----------------------------------

.. list of all pictures / menu_sur_vue
.. |machine| image:: _static/img/machine.png
.. |machine_selected| image:: _static/img/machine_selected.png
.. |machine_rc_base| image:: _static/img/machine_rigth_click_base_en.png
.. |machine_rc_ref| image:: _static/img/machine_rigth_click_notdel_en.png
.. |machine_rc_dup| image:: _static/img/machine_rigth_click_en.png


.. |machine_duplicate| image:: _static/img/machine_rigth_click_dup_en.png
.. |machine_delete| image:: _static/img/machine_rigth_click_del_en.png
.. |machine_resized| image:: _static/img/machine_resize.png
.. |ref_machine_not_delete| image:: _static/img/machine_rigth_click_notdel_en.png
.. |machine_locked| image:: _static/img/machine_locked.png

.. |design_cell_view| image:: _static/img/process/factory_design_cell_view.png
.. |design_cell_tronq_view| image:: _static/img/process/factory_design_cell_tronq_view.png


.. -----------------------------box main menu-----------------------------------

.. list of all pictures / box main menu / parameter
.. |box_parameters| image:: _static/img/box_parameters_en.png

.. list of all pictures / box main menu / reroutage
.. |message_reroutage_base_case| image:: _static/img/message_rerouting_base_case_en.png
.. |box_reroutage_open| image:: _static/img/box_rerouting_en.png
.. |box_reroutage_sample| image:: _static/img/box_rerouting_sample_en.png
.. |box_reroutage_selection| image:: _static/img/box_rerouting_en.png
.. |box_reroutage_selection_old| image:: _static/img/box_rerouting_old_en.png
.. |box_reroutage_selection_new| image:: _static/img/box_rerouting_new_en.png
.. |box_reroutage_selection_from| image:: _static/img/box_rerouting_from_en.png
.. |box_reroutage_selection_to| image:: _static/img/box_rerouting_to_en.png

.. list of all pictures / box main menu / filter
.. |box_filter_product| image:: _static/img/box_filter_product_en.png
.. |box_filter_product_product| image:: _static/img/box_filter_product_product_en.png
.. |box_filter_product_family| image:: _static/img/box_filter_product_family_en.png
.. |box_filter_product_pareto| image:: _static/img/box_filter_product_pareto_en.png
.. |box_filter_product_info| image:: _static/img/box_filter_product_info_en.png
.. |box_filter_machine| image:: _static/img/box_filter_machine_en.png

.. list of all pictures / box main menu / cell optimizer
.. |box_optimizer_open| image:: _static/img/box_optimizer_open_en.png
.. |box_optimizercell| image:: _static/img/box_optimizercell_en.png
.. |box_optimizercell_base| image:: _static/img/box_optimizer_base_en.png
.. |box_optimizercell_sc| image:: _static/img/box_optimizer_sc_en.png

.. list of all pictures / box main menu / apply solution
.. |box_applysolution| image:: _static/img/box_optimizer_apply_en.png

.. -----------------------------traitement complet-----------------------------------

.. list of all pictures / traitement complet /
.. |message_dialog_openfail| image:: _static/img/message_dialog_openfail_en.png
.. |message_close| image:: _static/img/message_close_en.png
.. |panel_lateral_scenario_select_rv| image:: _static/img/panel_lateral_scenario_select_rv_en.png
.. |message_parent_selection| image:: _static/img/message_parent_selection_en.png
.. |panel_lateral_structure_select_comp| image:: _static/img/panel_lateral_structure_select_comp.png
.. |panel_lateral_structure_select_site| image:: _static/img/panel_lateral_structure_select_site.png
.. |panel_lateral_structure_select_building| image:: _static/img/panel_lateral_structure_select_building.png

.. |process_real_1_map| image:: _static/img/process/vue_real/process_real_1_map.png
.. |process_real_2_factory| image:: _static/img/process/vue_real/process_real_2_factory.png
.. |process_real_3_cell| image:: _static/img/process/vue_real/process_real_3_cell.png
.. |process_real_4_skeleton| image:: _static/img/process/vue_real/process_real_4_skeleton.png
.. |process_real_5_io| image:: _static/img/process/vue_real/process_real_5_io.png
.. |process_real_6_scale| image:: _static/img/process/vue_real/process_real_6_scale_en.png
.. |process_real_7_setvalue| image:: _static/img/process/vue_real/process_real_7_setvalue_en.png
.. |process_real_8_graph| image:: _static/img/process/vue_real/process_real_8_graph.png
.. |process_real_9_machine_resize| image:: _static/img/process/vue_real/process_real_9_machine_resize.png
.. |process_real_10_machine_lock| image:: _static/img/process/vue_real/process_real_10_machine_lock.png
.. |process_real_11_dashboard| image:: _static/img/process/vue_real/process_real_11_dashboard_en.png

.. |process_graph1| image:: _static/img/process/vue_graph/graph1.png
.. |process_graph2| image:: _static/img/process/vue_graph/graph2.png
.. |process_graph3| image:: _static/img/process/vue_graph/graph3.png
.. |process_graph4| image:: _static/img/process/vue_graph/graph4.png
.. |process_graph5| image:: _static/img/process/vue_graph/graph5.png
.. |process_graph6| image:: _static/img/process/vue_graph/graph6.png
.. |process_graph7| image:: _static/img/process/vue_graph/graph7.png
.. |process_graph8| image:: _static/img/process/vue_graph/graph8.png
.. |process_graph9| image:: _static/img/process/vue_graph/graph9.png
.. |process_graph10| image:: _static/img/process/vue_graph/graph10.png
.. |process_graph11| image:: _static/img/process/vue_graph/graph11.png
.. |process_graph12| image:: _static/img/process/vue_graph/graph12.png
.. |process_graph13| image:: _static/img/process/vue_graph/graph13.png

.. |process_cell1| image:: _static/img/process/vue_cell/cell1.png
.. |process_cell2| image:: _static/img/process/vue_cell/cell2.png
.. |process_cell3| image:: _static/img/process/vue_cell/cell3.png

.. |view_cell_0| image:: _static/img/view_cell_0.png
.. |view_cell_1| image:: _static/img/view_cell_1.png
.. |view_cell_2| image:: _static/img/view_cell_2.png
.. |view_cell_3| image:: _static/img/view_cell_3.png
.. |view_cell_4| image:: _static/img/view_cell_4.png
.. |view_cell_5| image:: _static/img/view_cell_5.png
.. |view_cell_6| image:: _static/img/view_cell_6.png
.. |view_cell_7| image:: _static/img/view_cell_7.png
.. |view_cell_10| image:: _static/img/view_cell_10.png

.. |skeletonpoint_connected| image:: _static/img/skeletonpoint_connected.png
.. |skeletonpoint_notconnected| image:: _static/img/skeletonpoint_notconnected.png
.. |skeletonpoint_valide| image:: _static/img/skeletonpoint_valide.png
.. |panel_lateral_machine_notconnected| image:: _static/img/panel_lateral_machine_notconnected_en.png



.. -----------------------------scheduler-----------------------------------
.. list of all pictures / scheduler /
.. |scheduler_menu_global_1| image:: _static/img/scheduler/scheduler_menu_global_1.png
.. |scheduler_menu_global_2| image:: _static/img/scheduler/scheduler_menu_global_2.png
.. |scheduler_menu_global_3| image:: _static/img/scheduler/scheduler_menu_global_3.png
.. |scheduler_menu_machine_1| image:: _static/img/scheduler/scheduler_menu_machine_1.png
.. |scheduler_menu_machine_2| image:: _static/img/scheduler/scheduler_menu_machine_2.png
.. |scheduler_menu_operation_1| image:: _static/img/scheduler/scheduler_menu_operation_1.png
.. |scheduler_menu_operation_2| image:: _static/img/scheduler/scheduler_menu_operation_2.png
.. |scheduler_menu_order_1| image:: _static/img/scheduler/scheduler_menu_order_1.png
.. |scheduler_menu_order_2| image:: _static/img/scheduler/scheduler_menu_order_2.png
.. |scheduler_menu_product| image:: _static/img/scheduler/scheduler_menu_product.png
.. |scheduler_menu_shift| image:: _static/img/scheduler/scheduler_menu_shift.png

.. |sched_result_gantt| image:: _static/img/scheduler/sched_result_gantt.png
.. |sched_result_KPI| image:: _static/img/scheduler/sched_result_KPI.png
.. |sched_result_KPIs| image:: _static/img/scheduler/sched_result_KPIs.png
.. |sched_result_lateness| image:: _static/img/scheduler/sched_result_lateness.png
.. |sched_result_leadTime| image:: _static/img/scheduler/sched_result_leadTime.png
.. |sched_result_machineUsage1| image:: _static/img/scheduler/sched_result_machineUsage1.png
.. |sched_result_machineUsage2| image:: _static/img/scheduler/sched_result_machineUsage2.png
.. |sched_result_machineUsage3| image:: _static/img/scheduler/sched_result_machineUsage3.png
.. |sched_result_parameters| image:: _static/img/scheduler/sched_result_parameters.png
.. |sched_result_startEndTime| image:: _static/img/scheduler/sched_result_startEndTime.png
.. |sched_result_waitTime| image:: _static/img/scheduler/sched_result_waitTime.png
.. |sched_result_waitTimeTimeline| image:: _static/img/scheduler/sched_result_waitTimeTimeline.png
.. |sched_menu_config| image:: _static/img/scheduler/sched_menu_config.png
.. |sched_menu_ordersGen| image:: _static/img/scheduler/sched_menu_ordersGen.png
.. |sched_menu_ordersGroup| image:: _static/img/scheduler/sched_menu_ordersGroup.png
.. |sched_menu_ordersPrio| image:: _static/img/scheduler/sched_menu_ordersPrio.png
.. |sched_menu_ordOps| image:: _static/img/scheduler/sched_menu_ordOps.png
.. |sched_results_dashboard| image:: _static/img/scheduler/sched_results_dashboard.png

.. -----------------------------data panel-----------------------------------
.. list of all screenshots / data /
.. |datapanel| image:: _static/img/datapanel/datapanel.png
.. |calendar| image:: _static/img/datapanel/calendar.png
.. |weekly_template| image:: _static/img/datapanel/weekly_template.png
.. |weekly_template_new_dup| image:: _static/img/datapanel/weekly_template_new_dup.png
.. |consumables| image:: _static/img/datapanel/consumables.png
.. |equipments| image:: _static/img/datapanel/equipments.png
.. |equipment_zone| image:: _static/img/datapanel/equipment_zone.png
.. |generate_event| image:: _static/img/datapanel/generate_event.png
.. |machine_calendar| image:: _static/img/datapanel/machine_calendar.png
.. |machine_type_change| image:: _static/img/datapanel/machine_type_change.png
.. |machines_and_types| image:: _static/img/datapanel/machines_and_types.png
.. |new_machines| image:: _static/img/datapanel/new_machines.png
.. |new_machines_dialog| image:: _static/img/datapanel/new_machines_dialog.png
.. |new_template_change| image:: _static/img/datapanel/new_template_change.png
.. |operation_boule_info| image:: _static/img/datapanel/operation_boule_info.png
.. |operation_context_menu| image:: _static/img/datapanel/operation_context_menu.png
.. |operation_current_precedence| image:: _static/img/datapanel/operation_current_precedence.png
.. |operation_insert| image:: _static/img/datapanel/operation_insert.png
.. |operation_machine_change| image:: _static/img/datapanel/operation_machine_change.png
.. |operation_parralele| image:: _static/img/datapanel/operation_parralele.png
.. |operators| image:: _static/img/datapanel/operators.png
.. |precedence_change| image:: _static/img/datapanel/precedence_change.png
.. |product_context_menu| image:: _static/img/datapanel/product_context_menu.png
.. |products| image:: _static/img/datapanel/products.png
.. |shift_reduce| image:: _static/img/datapanel/shift_reduce.png
.. |shift_reduced| image:: _static/img/datapanel/shift_reduced.png
.. |calendrier-3-shift| image:: _static/img/datapanel/calendrier-3-shift.png
.. |calendrier-period-1day-capacity| image:: _static/img/datapanel/calendrier-period-1day-capacity.png
.. |calendrier-periode-3days-capacity| image:: _static/img/datapanel/calendrier-periode-3days-capacity.png
.. |calendrier-shift-action| image:: _static/img/datapanel/calendrier-shift-action.png
.. |data-scenario| image:: _static/img/datapanel/data-scenario.png
.. |equipments-equipments| image:: _static/img/datapanel/equipments-equipments.png
.. |equipments-equipments-tab| image:: _static/img/datapanel/equipments-equipments-tab.png
.. |equipments-equipments-types| image:: _static/img/datapanel/equipments-equipments-types.png
.. |machines-calendar-by-machine| image:: _static/img/datapanel/machines-calendar-by-machine.png
.. |machines-calendar-by-machine-events| image:: _static/img/datapanel/machines-calendar-by-machine-events.png
.. |machines-calendar-by-machine-overview| image:: _static/img/datapanel/machines-calendar-by-machine-overview.png
.. |machines-calendar-by-machine-overview-7-12| image:: _static/img/datapanel/machines-calendar-by-machine-overview-7-12.png
.. |machines-calendar-by-machine-overview-12-12| image:: _static/img/datapanel/machines-calendar-by-machine-overview-12-12.png
.. |machines-calendar-by-machine-overview-m1| image:: _static/img/datapanel/machines-calendar-by-machine-overview-m1.png
.. |machines-calendar-by-machine-overview-m2| image:: _static/img/datapanel/machines-calendar-by-machine-overview-m2.png
.. |machines-calendar-by-machine-overview-shift1| image:: _static/img/datapanel/machines-calendar-by-machine-overview-shift1.png
.. |machines-calendar-by-machine-overview-shift1-extended| image:: _static/img/datapanel/machines-calendar-by-machine-overview-shift1-extended.png
.. |machines-new-machines| image:: _static/img/datapanel/machines-new-machines.png
.. |machines-types-and-machines-machines| image:: _static/img/datapanel/machines-types-and-machines-machines.png
.. |calendar_global2| image:: _static/img/datapanel/calendar_global2.png
.. |operators-add| image:: _static/img/datapanel/operators-add.png
.. |operators-added| image:: _static/img/datapanel/operators-added.png
.. |operators-adding| image:: _static/img/datapanel/operators-adding.png
.. |operators-deleted| image:: _static/img/datapanel/operators-deleted.png
.. |operators-deleting| image:: _static/img/datapanel/operators-deleting.png
.. |operators-remove| image:: _static/img/datapanel/operators-remove.png
.. |operators-select| image:: _static/img/datapanel/operators-select.png
.. |operators-skills| image:: _static/img/datapanel/operators-skills.png
.. |products-exemple-choix-type| image:: _static/img/datapanel/products-exemple-choix-type.png
.. |products-exemple-fini| image:: _static/img/datapanel/products-exemple-fini.png
.. |products-exemple-insertlast| image:: _static/img/datapanel/products-exemple-insertlast.png
.. |products-exemple-newop-created| image:: _static/img/datapanel/products-exemple-newop-created.png
.. |products-exemple-precedence| image:: _static/img/datapanel/products-exemple-precedence.png
.. |products-exemple-rename| image:: _static/img/datapanel/products-exemple-rename.png
.. |products-exemple-subprod-created| image:: _static/img/datapanel/products-exemple-subprod-created.png
.. |products-operations| image:: _static/img/datapanel/products-operations.png
.. |products-precedences| image:: _static/img/datapanel/products-precedences.png
.. |products-precedences-info| image:: _static/img/datapanel/products-precedences-info.png
.. |products-products| image:: _static/img/datapanel/products-products.png
.. |products-products-create-subprod| image:: _static/img/datapanel/products-products-create-subprod.png
.. |products-subproducts| image:: _static/img/datapanel/products-subproducts.png

